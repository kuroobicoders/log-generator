# Ok, what is this?
Yeah, it is a little embarrasing. This application, basically, do nothing. It has been developed using Kotlin just as a resource of a bigger thing.

This application is intended to **generate a randomly number of log traces**. But, not complety randomly, just a little bit randomly ;). It exists as a resource for a Elastic Stack trainning that you can find on Udemy [Elastic Stack: Beats, Logstash, Elasticsearch y Kibana. Búsquedas, analítica de logs y monitorización de sitemas.](http://www.udemy.com "Elastic Stack: Beats, Logstash, Elasticsearch y Kibana. Búsquedas, analítica de logs y monitorización de sitemas.")

You can try to control some parameters in order to create as many number of traces as you expect to have.

# How to use it
```
usage: java -jar log-generator-[version].jar [-h] [-l LOOP] [-d DELAY]

optional arguments:
  -h, --help      show this help message and exit

  -l LOOP,        Number of iterations. Keep in mind that 5 traces can be
  --loop LOOP     generated randomly in each iteration. Use -1 for an
                  unlimited number of iterations. Default value: 1000

  -d DELAY,       Delay after each iteration (in milliseconds). Default
  --delay DELAY   values: 1000
```
# How to build
## On Linux / Mac
```[bash]
./gradlew build
```
## On Windows
```[bash]
gradlew.bat build
```